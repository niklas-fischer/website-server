<div align="center">

# My web server | niklas-fischer.de

[![Licence badge](https://img.shields.io/static/v1?label=LICENCE&message=A-GPL%20V3&labelColor=lightgrey&color=black&style=for-the-badge)](LICENCE)
[![Apache badge](https://img.shields.io/badge/Apache-orange?style=for-the-badge&logo=Apache&logoColor=white)](https://httpd.apache.org/)
[![Ansible badge](https://img.shields.io/badge/Ansible-red?style=for-the-badge&logo=Ansible&logoColor=white)](https://www.ansible.com/)
[![Vagrant badge](https://img.shields.io/badge/Vagrant-1868f2?style=for-the-badge&logo=Vagrant&logoColor=white)](https://www.vagrantup.com/)
[![Codeberg Badge](https://img.shields.io/badge/Codeberg-2185d0?style=for-the-badge&logo=codeberg&logoColor=white)](https://codeberg.org)
![Gluten free badge](https://img.shields.io/static/v1?label=Gluten&message=Free&labelColor=aa66cc&color=9933cc&style=for-the-badge)


**Glad to see you here!**

It is easy to install a web server, there are quadrillions of tutorials for this in the infinite expanses of the Internet. But what if you can't find the link again after you've wrecked your web server? Or what about a decent documentation?

Since I'm a lazy person and I like to tinker with things, I wrote an Ansible playbook based on current IT security standards for setting me up a minimal web server.

[Testing environment](#🖥️-testing-environment) •

</div>

# 🖥️ Testing environment

To setup your testing environment we use [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/). We assume that your distribution is based on Linux.

## Prerequisite Tools

- [Git](https://git-scm.com/)
- [Ansible](https://www.ansible.com/)

**Note** <br>
Since Ansible is agentless, installing the Ansible software is only necessary on the control host. You don't need anything on the target systems (except SSH and Python).

## Fetch from Codeberg

Go to your working directory and clone the repository

```console
local@supercomputer:~$ git clone https://codeberg.org/niklas-fischer/website/server.git
```
1. Navigate to the newly created directory `website-server` with:

```console
local@supercomputer:~$ cd website-server/
```

Open the files with a code editor of your choice.

## Setting up Vagrant and VirtualBox

After cloning the repository we are ready to install Vagrant and VirtualBox. If you are using Fedora, a proper script to install all necessary dependencies is under `scripts/`.

```console
local@supercomputer:~$ ./fedora-setup.sh
```
If you are using Linux on another distribution, refer to the installation instructions for [Vagrant](https://www.vagrantup.com/docs/installation) and [VirtualBox](https://www.virtualbox.org/manual/ch02.html).

## Running

Run this command to startup Vagrant:

```console
local@supercomputer:~$ vagrant up
```
and login via:
```console
local@supercomputer:~$ vagrant ssh [hostname]
```
After setting up the VM with the playbook role, you can't login with `vagrant ssh`. Use 
```console
local@supercomputer:~$ ssh [username]@192.168.150.10
```
instead.

## Troubleshooting

If you're running into difficulties, look here.

### IP address outside of allowed range

If the specified IP address for VirtualBox is outside the allowed range, using the *vagrant up* command will result in this error message:

    The IP address configured for the host-only network is not within the allowed ranges. Please update the address used to be within the allowed ranges and run the command again.
    
    Address: 192.168.150.10
    Ranges: 192.168.56.0/21

    Valid ranges can be modified in the /etc/vbox/networks.conf file. For
    more information including valid format see:

    https://www.virtualbox.org/manual/ch06.html#network_hostonly

The way to solve is creating a new file at `/etc/vbox/networks.conf` on your system containing

```console
* 10.0.0.0/8 192.168.0.0/16
* 2001::/64
```

Make sure including the **asterisks** `*`. Then the issue should be gone.

### VM not starting/not provisioning properly/etc...

If certain VMs do not provision properly or do not start at all (this seems to depend on the day or the phase of the moon), a new attempt often helps:

```console
local@supercomputer:~$ vagrant reload <VM_NAME>
```

<p align="right">(<a href="#top">back to top</a>)</p>